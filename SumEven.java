import java.io.*;

interface Sum{
    public int calc(int[] ar, int n);
}

public class SumEven{
    public static void main(String[] args){
        int n = Integer.parseInt(args[0]);
        int ar[] = new int[n];
        for(int i=0;i<n;i++){
            ar[i] = Integer.parseInt(args[i+1]);
        }
        Sum ans[] = new Sum[1];
        ans[0] = (a,s) -> {
                int answer = 0;
                for(int i=0;i<s;i++){
                    if(a[i]%2==0)
                        answer+=a[i];
                }
                return answer;
        };
        System.out.println(ans[0].calc(ar,n));
    }
}